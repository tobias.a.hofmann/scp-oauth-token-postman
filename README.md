# SCP OAuth token Postman

Obtain an OAuth 2.0 token from SAP Cloud Platform Neo with Postman

Before you can use this (dummy) request, you must set the OAuth 2.0 data. Currently, Postman cannot export these [Save OAuth2.0 New Access Token parameters with collection](https://github.com/postmanlabs/postman-app-support/issues/5459)

Token Name: scpoauthtoken
Grant Type: Client Credentials
Access Token URL: See SCP
Client ID: See SCP OAuth client configuration
Client Secret: See SCP OAuth client configuration
Scope: See SCP Java app

![Postman parameters](PostmanGetNewAccessToken.png "Postman parameters")

